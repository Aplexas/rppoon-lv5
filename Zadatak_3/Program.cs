﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Zadatak_3
{
    class Program
    {
        static void Main(string[] args)
        {
            IDataset virtualProxy = new VirtualProxyDataset("sensitiveData.txt");
            IDataset protectionProxy = new ProtectionProxyDataset(User.GenerateUser("userWithoutAccess"));

            DataConsolePrinter dataConsolePrinter = new DataConsolePrinter();
            dataConsolePrinter.Print(protectionProxy);
            dataConsolePrinter.Print(virtualProxy);
        }
    }
}
