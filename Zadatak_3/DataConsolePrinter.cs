﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak_3
{
    class DataConsolePrinter
    {

        public void Print(IDataset proxy)
        {
            foreach (List<string> subList in proxy.GetData())
            {
                foreach (string data in subList)
                {
                    Console.WriteLine(data);
                }
            }
        }
    }
}
