﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak_6
{
    class GroupNote:Note
    {
        private List<string> peopleInGroup; 
        public GroupNote(string message, ITheme theme) : base(message, theme) { this.peopleInGroup = new List<string>(); }
        public override void Show()
        {
            this.ChangeColor();
            Console.WriteLine("Note to the group: ");
            string framedMessage = this.GetFramedMessage();
            Console.WriteLine(framedMessage);
            Console.ResetColor();
        }
           
        public void AddMember(string name)
        { 
            this.peopleInGroup.Add(name);
            Console.WriteLine("Member "+ name+" added");
        }

        public void RemoveMember(string name)
        {
            foreach(string member in peopleInGroup)
            {
                if (member == name)
                {
                    peopleInGroup.Remove(member);
                    Console.WriteLine("Member " + name + " removed");
                    return;
                }
            }
            Console.WriteLine(name + " is not member of group");
        }
    }
}
