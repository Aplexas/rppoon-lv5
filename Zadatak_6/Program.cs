﻿using System;

namespace Zadatak_6
{
    class Program
    {
        static void Main(string[] args)
        {
            ITheme lightTheme = new LightTheme();
            ITheme happyTheme = new HappyTheme();
            GroupNote groupNote = new GroupNote("TesttesT", happyTheme);
            groupNote.AddMember("Toni");
            groupNote.AddMember("Marko");
            groupNote.AddMember("Pavao");
            groupNote.RemoveMember("Marko");
            groupNote.RemoveMember("Jack");
            groupNote.Show();

            
        }
    }
}
