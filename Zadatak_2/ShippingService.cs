﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak_2
{
    class ShippingService
    {
        private double costPerWeight = 1;

        public ShippingService(double costPerWeight)
        {
            this.costPerWeight = costPerWeight;
        }

        public double calculatePrice(IShipable box)
        {
            double totalPrice = box.Weight * costPerWeight;
            return totalPrice;
        }

        public double CostPerWeight {
            get{ return this.costPerWeight; }
            set { this.costPerWeight = value; }
        }
    }
}
