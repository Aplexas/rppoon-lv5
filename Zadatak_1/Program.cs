﻿using System;

namespace Zadatak_1
{
    class Program
    {
        static void Main(string[] args)
        {
            IShipable item1 = new Product("item1",5,29);
            IShipable item2 = new Product("item2", 15,49);
            IShipable item3 = new Product("item3", 25,12);
            Box box = new Box("kutija");
            box.Add(item1);
            box.Add(item2);
            box.Add(item3);
            Console.WriteLine(box.Description());
        }
    }
}
