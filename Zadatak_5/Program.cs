﻿using System;

namespace Zadatak_5
{
    class Program
    {
        static void Main(string[] args)
        {
            ITheme lightTheme = new LightTheme();
            ITheme happyTheme = new HappyTheme(); 
            ReminderNote reminderNote = new ReminderNote("TesttesT", happyTheme);
            reminderNote.Show();
        }
    }
}
