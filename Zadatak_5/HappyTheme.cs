﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak_5
{
    class HappyTheme:ITheme
    {
        public void SetBackgroundColor()
        {
            Console.BackgroundColor = ConsoleColor.Magenta;
        }
        public void SetFontColor()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
        }
        public string GetHeader(int width)
        {
            return new string('~', width);
        }
        public string GetFooter(int width)
        {
            return new string(')', width);
        }
    }
}
