﻿using System;

namespace Zadatak_7
{
    class Program
    {
        static void Main(string[] args)
        {
            ITheme lightTheme = new LightTheme();
            ITheme happyTheme = new HappyTheme();
            GroupNote groupNote = new GroupNote("Group note", happyTheme);
            ReminderNote reminderNote = new ReminderNote("Reminder note", happyTheme);
            groupNote.AddMember("Toni");
            groupNote.AddMember("Marko");
            Notebook notebook = new Notebook(lightTheme);
            notebook.AddNote(groupNote);
            notebook.AddNote(reminderNote);
            notebook.Display();
            notebook.ChangeTheme(happyTheme);
            notebook.Display();
        }
    }
}
